FROM mcr.microsoft.com/dotnet/core/aspnet:2.2-stretch-slim AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/core/sdk:2.2-stretch AS build
WORKDIR /src
COPY ["Rabbit/Rabbit.csproj", "Rabbit/"]
COPY ["EventBusServiceBus/EventBusServiceBus.csproj", "EventBusServiceBus/"]
COPY ["EventBus/EventBus.csproj", "EventBus/"]
COPY ["EventBusRabbitMQ/EventBusRabbitMQ.csproj", "EventBusRabbitMQ/"]
RUN dotnet restore "Rabbit/Rabbit.csproj"
COPY . .
WORKDIR "/src/Rabbit"
RUN dotnet build "Rabbit.csproj" -c Release -o /app

FROM build AS publish
RUN dotnet publish "Rabbit.csproj" -c Release -o /app

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "Rabbit.dll"]
