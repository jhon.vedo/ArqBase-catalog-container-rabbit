﻿using Microsoft.eShopOnContainers.BuildingBlocks.EventBus.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rabbit.Model
{
    public class Catalog: IntegrationEvent
    {        
        public string Nombre { get; set; }
    }
}
