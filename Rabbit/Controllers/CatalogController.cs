﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Rabbit.Integrations;
using Rabbit.Model;

namespace Rabbit.Controllers
{
    [Route("api/v1/[controller]")]
    public class CatalogController : Controller
    {
        private readonly ICatalogIntegrationEventService _catalogIntegrationEventService;

        public CatalogController(ICatalogIntegrationEventService catalogIntegrationEventService)
        {
            _catalogIntegrationEventService = catalogIntegrationEventService;
        }

        [Route("items")]
        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        public async Task<ActionResult> CreateCatalogAsync([FromBody]Catalog catalog)
        {

          //  await _catalogIntegrationEventService.SaveEventAndCatalogContextChangesAsync(catalog);
            // Publish through the Event Bus and mark the saved event as published
            await _catalogIntegrationEventService.PublishThroughEventBusAsync(catalog);
            return Ok();
        }       
       
    }
}