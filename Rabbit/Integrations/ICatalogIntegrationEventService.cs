﻿using Microsoft.eShopOnContainers.BuildingBlocks.EventBus.Abstractions;
using Microsoft.eShopOnContainers.BuildingBlocks.EventBus.Events;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;

namespace Rabbit.Integrations
{
    public interface ICatalogIntegrationEventService
    {
        Task SaveEventAndCatalogContextChangesAsync(IntegrationEvent evt);
        Task PublishThroughEventBusAsync(IntegrationEvent evt);
    }
    public class CatalogIntegrationEventService : ICatalogIntegrationEventService
    {
       // private readonly Func<DbConnection, IIntegrationEventLogService> _integrationEventLogServiceFactory;
        private readonly IEventBus _eventBus;
      //  private readonly CatalogContext _catalogContext;
      //  private readonly IIntegrationEventLogService _eventLogService;
      //  private readonly ILogger<CatalogIntegrationEventService> _logger;

        public CatalogIntegrationEventService(
        //    ILogger<CatalogIntegrationEventService> logger,
            IEventBus eventBus
         //   CatalogContext catalogContext,
         //   Func<DbConnection, IIntegrationEventLogService> integrationEventLogServiceFactory
         )
        {
          //  _logger = logger ?? throw new ArgumentNullException(nameof(logger));
         //   _catalogContext = catalogContext ?? throw new ArgumentNullException(nameof(catalogContext));
        //    _integrationEventLogServiceFactory = integrationEventLogServiceFactory ?? throw new ArgumentNullException(nameof(integrationEventLogServiceFactory));
            _eventBus = eventBus ?? throw new ArgumentNullException(nameof(eventBus));
         //   _eventLogService = _integrationEventLogServiceFactory(_catalogContext.Database.GetDbConnection());
        }

        public async Task PublishThroughEventBusAsync(IntegrationEvent evt)
        {
            try
            {
               
                _eventBus.Publish(evt);
               
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task SaveEventAndCatalogContextChangesAsync(IntegrationEvent evt)
        {
            //_logger.LogInformation("----- CatalogIntegrationEventService - Saving changes and integrationEvent: {IntegrationEventId}", evt.Id);

            ////Use of an EF Core resiliency strategy when using multiple DbContexts within an explicit BeginTransaction():
            ////See: https://docs.microsoft.com/en-us/ef/core/miscellaneous/connection-resiliency            
            //await ResilientTransaction.New(_catalogContext).ExecuteAsync(async () =>
            //{
            //    // Achieving atomicity between original catalog database operation and the IntegrationEventLog thanks to a local transaction
            //    await _catalogContext.SaveChangesAsync();
            //    await _eventLogService.SaveEventAsync(evt, _catalogContext.Database.CurrentTransaction);
            //});
        }
    }
}
