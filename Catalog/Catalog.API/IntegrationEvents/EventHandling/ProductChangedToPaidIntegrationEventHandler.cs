﻿namespace Microsoft.eShopOnContainers.Services.Catalog.API.IntegrationEvents.EventHandling
{
    using BuildingBlocks.EventBus.Abstractions;
    using System.Threading.Tasks;
    using Infrastructure;
    using Microsoft.eShopOnContainers.Services.Catalog.API.IntegrationEvents.Events;
    using Microsoft.Extensions.Logging;
    using Serilog.Context;

    public class ProductChangedToPaidIntegrationEventHandler : 
        IIntegrationEventHandler<ProductPriceChangedIntegrationEvent>
    {
      
        public ProductChangedToPaidIntegrationEventHandler()
        {
           
        }

       

        public async Task Handle(ProductPriceChangedIntegrationEvent @event)
        {
           
        }
    }
}